export default {
	// Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
	ssr: true,

	// Target: https://go.nuxtjs.dev/config-target
	target: "static",

	// Global page headers: https://go.nuxtjs.dev/config-head
	head: {
		title: "Tinygram",
		htmlAttrs: {
			lang: "en",
		},
		meta: [
			{
				charset: "utf-8",
			},
			{
				name: "viewport",
				content: "width=device-width, initial-scale=1",
			},
			{
				hid: "description",
				name: "description",
				content: "",
			},
			{
				name: "format-detection",
				content: "telephone=no",
			},
			{
				name: "google-signin-client_id",
				content: `253955023096-st84p90v2994sqffqg6c1vj45iah8sci.apps.googleusercontent.com`
			}
		],
		link: [
			{
				rel: "icon",
				type: "image/x-icon",
				href: "/favicon.ico",
			},
		],
		script: [
			{
				src: "https://apis.google.com/js/platform.js",
				async: true,
				defer: true,
			},
			{
				src: "../google.js",
			}
		]
	},

	styleResources: {
		scss: ["./assets/scss/style.scss"],
	},

	// Auto import components: https://go.nuxtjs.dev/config-components
	components: true,

	// Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
	buildModules: [
		// https://go.nuxtjs.dev/eslint
		"@nuxtjs/eslint-module",
		"@nuxtjs/dotenv",
		"@nuxt/image",
	],

	dayjs: {
		locales: ["en"],
	},
	// Modules: https://go.nuxtjs.dev/config-modules
	modules: [
		// https://go.nuxtjs.dev/axios
		"@nuxtjs/axios",
		"@nuxtjs/style-resources",
		"@nuxtjs/dayjs",
	],

	// Axios module configuration: https://go.nuxtjs.dev/config-axios
	axios: {},

	// Build Configuration: https://go.nuxtjs.dev/config-build
	build: {},
}
