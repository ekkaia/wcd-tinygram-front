const sendPost = async (description, url, userId) => {
	try {
		await fetch(`https://tiny-gram-wcd.oa.r.appspot.com/api/v1/posts?image_url=${url}&user=${userId}&description=${description}`, {
			method: "POST"
		});
	} catch (err) {
		throw new Error(err);
	}
};

const getFeed = async (userId) => {
	try {
		const res = await fetch(`https://tiny-gram-wcd.oa.r.appspot.com/api/v1/posts/feed?user=${userId}`, {
			method: "GET"
		});
		return res;
	} catch(err) {
		throw new Error(err);
	}
};

const getProfile = async (userId) => {
	try {
		const res = await fetch(`https://tiny-gram-wcd.oa.r.appspot.com/api/v1/members/profile?user=${userId}`, {
			method: "GET"
		});
		return res;
	} catch(err) {
		throw new Error(err);
	}
};

const getFollowsCount = async (userId) => {
	try {
		const res = await fetch(`https://tiny-gram-wcd.oa.r.appspot.com/api/v1/members/followers_count?user=${userId}`, {
			method: "GET"
		});
		return res;
	} catch(err) {
		throw new Error(err);
	}
};

const getFollowsList = async (userId) => {
	try {
		const res = await fetch(`https://tiny-gram-wcd.oa.r.appspot.com/api/v1/members/follows?user=${userId}`, {
			method: "GET"
		});
		return res;
	} catch(err) {
		throw new Error(err);
	}
};

const follow = async (userId, targetUserId) => {
	try {
		const res = await fetch(`https://tiny-gram-wcd.oa.r.appspot.com/api/v1/members/follow?user=${userId}&target=${targetUserId}`, {
			method: "POST"
		});
		return res;
	} catch(err) {
		throw new Error(err);
	}
};

const unfollow = async (userId, targetUserId) => {
	try {
		const res = await fetch(`https://tiny-gram-wcd.oa.r.appspot.com/api/v1/members/follow?user=${userId}&target=${targetUserId}`, {
			method: "DELETE"
		});
		return res;
	} catch(err) {
		throw new Error(err);
	}
};

const likeAPost = async (postId) => {
	try {
		await fetch(`https://tiny-gram-wcd.oa.r.appspot.com/api/v1/posts/like?post=${postId}`, {
			method: "PUT"
		});
	} catch(err) {
		throw new Error(err);
	}
};

const getUserPosts = async (userId) => {
	try {
		const res = await fetch(`https://tiny-gram-wcd.oa.r.appspot.com/api/v1/members/posts?user=${userId}`, {
			method: "GET"
		});
		return res;
	} catch(err) {
		throw new Error(err);
	}
};

const getPost = async (postId) => {
	try {
		const res = await fetch(`https://tiny-gram-wcd.oa.r.appspot.com/api/v1/posts?post=${postId}`, {
			method: "GET"
		});
		return res;
	} catch(err) {
		throw new Error(err);
	}
};

const api = {};
api.sendPost = sendPost;
api.getFeed = getFeed;
api.getProfile = getProfile;
api.getFollowsCount = getFollowsCount;
api.getFollowsList = getFollowsList;
api.follow = follow;
api.unfollow = unfollow;
api.likeAPost = likeAPost;
api.getUserPosts = getUserPosts;
api.getPost = getPost;
export default api;
