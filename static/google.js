// eslint-disable-next-line no-unused-vars
async function onSignIn(googleUser) {
	const imageURL = googleUser.getBasicProfile().getImageUrl();
	const token = googleUser.getAuthResponse(true).access_token;

	try {
		await fetch(`https://tiny-gram-wcd.oa.r.appspot.com/api/v1/auth/login?user=${token}&image_url=${imageURL}`, {
			method: "POST",
		});
	} catch(err) {
		alert(err);
	}

	localStorage.setItem("user_id", googleUser.getBasicProfile().getId());
	window.location.replace("/");
}
